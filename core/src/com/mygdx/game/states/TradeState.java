package com.mygdx.game.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.FlappyDemo;

/**
 * Created by Дмитрий on 24.12.2016.
 */

    public class TradeState extends State{
        private Texture background;
        private Texture tradeState;

        public TradeState(GameStateMenager gam) {
            super(gam);
            background = new Texture("Loks.png");
            tradeState = new Texture("MenuTrade.png");
        }

    @Override
    protected void handleinput() {
          if(Gdx.input.justTouched()){
              game.set(new PlayState(game));
          }
    }

    @Override
    public void update(float dt) {

    }

    @Override
    public void render(SpriteBatch sb) {
         sb.begin();
         sb.draw(tradeState,FlappyDemo.WIDTH,FlappyDemo.HEIGHT);
         sb.end();
    }

    @Override
    public void dispose() {
        background.dispose();
        tradeState.dispose();
    }
}
