package com.mygdx.game.states;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.Stack;

/**
 * Created by Дмитрий on 20.12.2016.
 */

public class GameStateMenager  {
    private Stack<State> states;
    public GameStateMenager(){
        states = new Stack<State>();
    }
    public void push(State state){
        states.push(state);

    }
    public void pop(){
        states.pop().dispose();
    }
    public void set(State state){
        states.pop().dispose();
        states.push(state);
    }
    public void update(float dt){
        states.peek().update(dt);
    }
    public void render(SpriteBatch sb){
        states.peek().render(sb);
    }
}
