package com.mygdx.game.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.FlappyDemo;

/**
 * Created by Дмитрий on 20.12.2016.
 */

public class MenuState extends State {
    private Texture background;
    private Texture playBtn;
    public MenuState(GameStateMenager gam) {
        super(gam);
        background = new Texture("Loks.png");
        playBtn = new Texture("StartBt.png");
    }

    @Override
    protected void handleinput() {
       if(Gdx.input.justTouched()){
           game.set(new PlayState(game));
       }
    }

    @Override
    public void update(float dt) {
handleinput();
    }

    @Override
    public void render(SpriteBatch sb) {
  sb.begin();
        sb.draw(background,0,0, FlappyDemo.WIDTH,FlappyDemo.HEIGHT);
        sb.draw(playBtn,(FlappyDemo.WIDTH/2)-(playBtn.getWidth()/2),(FlappyDemo.HEIGHT /2));
        sb.end();

    }

    @Override
    public void dispose() {
background.dispose();
        playBtn.dispose();
    }
}
