package com.mygdx.game.states;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.FlappyDemo;
import com.mygdx.game.sprites.Enemy;
import com.mygdx.game.sprites.Hero;
import com.mygdx.game.sprites.Trader;

import static com.mygdx.game.sprites.Enemy.LIFEENEMY;
import static com.mygdx.game.sprites.Enemy.enemyAnimationLeft;
import static com.mygdx.game.sprites.Enemy.enemyAnimationRight;
import static com.mygdx.game.sprites.Enemy.enemyil;
import static com.mygdx.game.sprites.Hero.heroAnimationLeft;
import static com.mygdx.game.sprites.Hero.heroAnimationRight;


/**
 * Created by Дмитрий on 20.12.2016.
 */

public class PlayState extends State {
    private Hero hero;
    private Trader trader;
    private Enemy enemy;
    private Texture bg;
    private int xTouch;
    private int yTouch;
    private int i = 0;
    private boolean proverka;
    private boolean proverkaattack;
    private float xAttack;
    private float yAttack;



    public PlayState(GameStateMenager gam) {
        super(gam);
        hero = new Hero(0, 348);
        trader = new Trader(Trader.X,Trader.Y);
        enemy = new Enemy(400, 348);
        camera.setToOrtho(false, 480, 800);
        bg = new Texture("Loks.png");
        Gdx.input.setInputProcessor(new Move());
    }

    @Override
    protected void handleinput() {
    }

    @Override
    public void update(float dt) {
        handleinput();
        if (xTouch>200 && xTouch <250) {
            hero.jump();

        }
        if (hero.getPosition().x < -6) {
            hero.getPosition().x = -6;
        } else if (hero.getPosition().x > 424) {
            hero.getPosition().x = 424;
        }
        hero.update(dt);
        enemy.update(dt);

        if (proverka == true && xTouch >350) {
            heroAnimationRight.update(dt);
            hero.getPosition().x += 200 * Gdx.graphics.getDeltaTime();
            if (camera.position.x > hero.getPosition().x) {
                camera.position.x = hero.getPosition().x;

            }
            camera.position.x += 3.33f;
            camera.update();
        } else if (proverka == true && xTouch <100)

        {
            hero.getPosition().x -= 200 * Gdx.graphics.getDeltaTime();
            heroAnimationLeft.update(dt);
            if (camera.position.x < hero.getPosition().x) {
                camera.position.x = hero.getPosition().x;
            }
            camera.position.x -= 3.33f;
            camera.update();
        }
        if (hero.getPosition().x - enemy.getPositionEnemy().x < 100 & hero.getPosition().x - enemy.getPositionEnemy().x > -5) {
            enemy.getPositionEnemy().x += 100 * Gdx.graphics.getDeltaTime();
            enemy.update(dt);
            if(xTouch < 250 && xTouch >200){
                enemy.jump();
            }
        } else if (enemy.getPositionEnemy().x - hero.getPosition().x < 100 & enemy.getPositionEnemy().x - hero.getPosition().x > -5) {
            enemy.getPositionEnemy().x -= 100 * Gdx.graphics.getDeltaTime();
            enemy.update(dt);
        }
        if (hero.getPosition().x - enemy.getPositionEnemy().x< 100 && hero.getPosition().x - enemy.getPositionEnemy().x>0) {
            enemyAnimationRight.update(dt);
        } else if (hero.getPosition().x - enemy.getPositionEnemy().x>-100 && hero.getPosition().x - enemy.getPositionEnemy().x<0) {
            enemyAnimationLeft.update(dt);
        }
        if(hero.getPosition().x-enemy.getPositionEnemy().x <32 && hero.getPosition().x-enemy.getPositionEnemy().x > -32 && proverkaattack == true){
              Enemy.LIFEENEMY = Enemy.LIFEENEMY-1;

        }
        if(hero.getPosition().x-trader.getPosition().x<50 && hero.getPosition().x - trader.getPosition().x>-40 ){

        }






    }


    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(camera.combined);
        sb.begin();
        sb.draw(bg, 0, 0, FlappyDemo.WIDTH, FlappyDemo.HEIGHT);
        sb.draw(trader.getTrader(),trader.getPosition().x,trader.getPosition().y);
        sb.draw(trader.getStore(),trader.getPosition().x-20,trader.getPosition().y+7);
        if (hero.getPosition().x - enemy.getPositionEnemy().x>-100 && hero.getPosition().x - enemy.getPositionEnemy().x<-5) {
            sb.draw(enemyAnimationLeft.getFrame(), enemy.getPositionEnemy().x, enemy.getPositionEnemy().y);
        }
        else if (hero.getPosition().x - enemy.getPositionEnemy().x< 100 && hero.getPosition().x - enemy.getPositionEnemy().x>5) {
            sb.draw(enemyAnimationRight.getFrame(), enemy.getPositionEnemy().x, enemy.getPositionEnemy().y);
        }else {
            sb.draw(enemyil, enemy.getPositionEnemy().x, enemy.getPositionEnemy().y);
        }
        if (proverka == true && xTouch >350) {
            sb.draw(heroAnimationRight.getFrame(), hero.getPosition().x, hero.getPosition().y);

        }
        else  if (proverka == true && xTouch <100) {
            sb.draw(heroAnimationLeft.getFrame(), hero.getPosition().x, hero.getPosition().y);
        }
        else {
            sb.draw(hero.getHero(), hero.getPosition().x, hero.getPosition().y);
        }
        sb.end();
    }
    @Override
    public void dispose() {

    }



    class Move implements InputProcessor {

        @Override
        public boolean keyDown(int keycode) {
            return false;
        }

        @Override
        public boolean keyUp(int keycode) {
            return false;
        }

        @Override
        public boolean keyTyped(char character) {
            return false;
        }

        @Override
        public boolean touchDown(int screenX, int screenY, int pointer, int button) {
            xTouch = screenX;
            yTouch = screenY;
            proverka = true;

            return false;
        }


        @Override
        public boolean touchUp(int screenX, int screenY, int pointer, int button) {
            proverka = false;
            if (trader.getPosition().x-screenX<40 && trader.getPosition().x-screenX > -40);
            i = 0;
            return false;
        }

        @Override
        public boolean touchDragged(int screenX, int screenY, int pointer) {
            proverka = false;
            xAttack = screenX;
            yAttack = screenY;
            proverkaattack = true;
            return false;
        }

        @Override
        public boolean mouseMoved(int screenX, int screenY) {
            return false;
        }

        @Override
        public boolean scrolled(int amount) {
            return false;
        }


    }
}

