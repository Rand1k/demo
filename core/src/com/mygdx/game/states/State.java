package com.mygdx.game.states;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.mygdx.game.sprites.Hero;

/**
 * Created by Дмитрий on 20.12.2016.
 */

public abstract class State {
    protected OrthographicCamera camera;
    protected Vector3 mouse;
    protected GameStateMenager game;
    public State (GameStateMenager gam){
        this.game = gam;
        camera = new OrthographicCamera();
        camera.position.x = 120;
        mouse = new Vector3();
    }
    protected abstract void handleinput();
    public abstract void update(float dt);
    public abstract void render(SpriteBatch sb);
    public abstract void dispose();

}
