package com.mygdx.game.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.TextureData;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.model.Animation;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Дмитрий on 20.12.2016.
 */

public class Hero {
    public static final int GRAVITY = -15;
    private Vector2 position;
    private Vector2 velosity;
    public static Texture hero;
    private Texture heroril;
    private Rectangle bounds;
    private Rectangle boundsattack;
    private Texture heroattack;
    static public AnimationHero heroAnimationRight;
    static public AnimationHero heroAnimationLeft;
    static public AnimationHero heroAttack;
    public Hero(int x, int y){
        position = new Vector2(x,y);
        velosity = new Vector2(0,0);
        hero = new Texture("animherotest.png");
        heroattack = new Texture("attack.png");
        heroril = new Texture("hero.png");
        heroAnimationRight = new AnimationHero(new TextureRegion(hero),4,0.5f);
        heroAnimationLeft = new AnimationHero(new TextureRegion(hero),4,0.5f);
        heroAttack = new AnimationHero(new TextureRegion(heroattack),3,0.5f);
        bounds = new Rectangle(x,y,hero.getWidth()/4,hero.getHeight()/4);
        boundsattack = new Rectangle(x,y,heroattack.getWidth()/3,heroattack.getHeight()/3);
    }

    public Texture getHero() {
        return heroril;
    }


    public Vector2 getPosition() {
        return position;
    }

    public void update(float dt){
        if(position.y>0)
           velosity.add(0,GRAVITY);
        velosity.scl(dt);
        position.add(0,velosity.y);
        velosity.scl(1/dt);
        if(position.y <348){
            position.y=348;
        }
    }
    public void jump(){
        velosity.y = 250;

    }
    public void dispose(){
        hero.dispose();
        heroattack.dispose();
    }
}
