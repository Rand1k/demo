package com.mygdx.game.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.TextureData;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.model.Animation;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Дмитрий on 20.12.2016.
 */

public class Enemy {
    public static int LIFEENEMY = 3;
    public static final int GRAVITY = -15;
    private Vector2 positionenemy;
    private Vector2 velosityenemy;
    private Texture enemy;
    static public Texture enemyil;
    private Rectangle boundsenemy;
    static public AnimationHero enemyAnimationRight;
    static public AnimationHero enemyAnimationLeft;
    public Enemy(int x,int y){
        positionenemy = new Vector2(x,y);
        velosityenemy = new Vector2(0,0);
        enemy = new Texture("enemiesLeft.png");
        enemyil = new Texture("Enemi.png");
        enemyAnimationRight = new AnimationHero(new TextureRegion(enemy),4,0.5f);
        enemyAnimationLeft = new AnimationHero(new TextureRegion(enemy),4,0.5f);
        boundsenemy = new Rectangle(x,y,enemy.getWidth()/4,enemy.getHeight()/4);
    }

    public Texture getEnemy() {
        return enemyil;
    }


    public Vector2 getPositionEnemy() {
        return positionenemy;
    }

    public void update(float dt){
        if(positionenemy.y>0)
            velosityenemy.add(0,GRAVITY);
        velosityenemy.scl(dt);
        positionenemy.add(0,velosityenemy.y);
        velosityenemy.scl(1/dt);
        if(positionenemy.y <342){
            positionenemy.y=344;
        }
    }
    public void jump(){
        velosityenemy.y = 150;

    }
    public void dispose(){
        enemy.dispose();
    }
}
