package com.mygdx.game.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Trader {
    private Texture trader;
    public static final int X = 200;
    public static final int Y = 336;

    private Texture store;
    private Vector2 position;
    private Rectangle traderbounds;
    private Rectangle shopbounds;
    public Trader(int x,int y){
        trader = new Texture("trader.png");
        position = new Vector2(0,0);
        position.add(X,Y);
        store = new Texture("shop.png");
        traderbounds = new Rectangle(X,Y,trader.getWidth(),trader.getHeight());
        shopbounds = new Rectangle(X,Y,store.getWidth(),store.getHeight());

    }
    public Texture getTrader() {
        return trader;
    }
    public Texture getStore(){return store;}
    public Vector2 getPosition() {
        return position;
    }


}
